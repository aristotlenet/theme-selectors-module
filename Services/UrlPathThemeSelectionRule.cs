﻿using System.Text.RegularExpressions;
using Orchard;
using Orchard.Environment.Extensions;
using Vandelay.Industries.Services;

namespace Contrib.ThemeSelectors.Services {
    [OrchardFeature("Contrib.ThemeSelectors")]
    public class UrlPathThemeSelectionRule : IThemeSelectionRule {
        private readonly IWorkContextAccessor _workContextAccessor;

        public UrlPathThemeSelectionRule(IWorkContextAccessor workContextAccessor) {
            _workContextAccessor = workContextAccessor;
        }

        public bool Matches(string name, string criterion) {
            var path = _workContextAccessor.GetContext().HttpContext.Request.Url.PathAndQuery;
            if (path == null) return false;

            if (path.StartsWith("/OrchardLocal")) {
                path = path.Replace("/OrchardLocal", "");
            }

            return new Regex(criterion, RegexOptions.IgnoreCase)
                .IsMatch(path);
        }
    }
}